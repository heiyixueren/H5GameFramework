/*
* name;
*/
class OpcodeEvent {
    public func: Function;
    public func_obj: any;

    constructor(func_obj: any, func: Function) {
        this.func = func;
        this.func_obj = func_obj;
    }
}

class Opcode {
    public static LOGIN = 1000;
    public static FAST_LOGIN = 1001;
    public static PING = 1010;
    public static LOBBY_GAME_LIST = 1110;
    public static NOTIFY_GAMEBEGIN = 5010;
    public static NOTIFY_CHIPIN = 5011;//下注通知
    public static REQ_WATCHCARD = 5012;//看牌请求及回调
    public static NOTIFY_WATCHCARD = 5013;//通知别人已看牌
    public static REQ_DISCARDCARD = 5014;//弃牌请求及回调
    public static NOTIFY_DISCARDCARD = 5015;//别人弃牌通知
    public static REQ_FOLLOWCHIP = 5016;//跟注请求及回调
    public static NOTIFY_FOLLOWCHIP = 5017;//通知别人已跟注
    public static REQ_ADDCHIP = 5018;//加注请求及回调
    public static NOTIFY_ADDCHIP = 5019;//通知别人已加注
    public static REQ_COMPARECARD = 5020;//比牌请求及回调
    public static NOTIFY_COMPARECARD = 5021;//比牌结果通知
    public static NOTIFY_GAMEND = 5022;//游戏结束通知
    public static NOTIFY_COINREFRESH = 1300;//更新玩家金币
    public static ME_MESSAGE = 1100;
    public static ENTER_ROOM = 1200;
    public static EXIT_ROOM = 1201;
    public static ROOM_INFO = 5001;//房间信息
    public static PLAYER_LIST = 5002;//房间成员列表
    public static ANNOUNCETION = 1112;//大厅美女公告
    public static MARQUEE = 1111;//跑马灯

    private event: Object = new Object;
    constructor() {

    }
    /**
     * 注册网络操作事件监听
     * @param cmd 事件id
     * @param func_obj 监听对象
     * @param func  回调函数
     */
    public register_event(cmd: number, func_obj: any, func: Function) {
        if (!this.event[cmd]) {
            this.event[cmd] = new Array<OpcodeEvent>();
            this.event[cmd].push(new OpcodeEvent(func_obj, func));
        }
        else {
            let arr: Array<OpcodeEvent> = this.event[cmd];
            for (let oe of arr) {
                let oeref: OpcodeEvent = oe;
                if (oeref.func_obj == func_obj) { //防止重复注册
                    return;
                }
            }
            this.event[cmd].push(new OpcodeEvent(func_obj, func));
        }
    }
    /**
     * 撤销对网络操作事件的监听
     * @param cmd 事件id
     * @param func_obj 监听对象 
     */
    public unregister_event(cmd: number, func_obj) {
        if (this.event[cmd]) {
            let arr: Array<OpcodeEvent> = this.event[cmd];
            for (var index = arr.length - 1; index >= 0; index--) {
                var oe = arr[index];
                if (oe.func_obj == func_obj) {
                    arr.splice(index, 1);//删除对象
                }
            }
        }
    }

    public call_event(cmd: number, data: any) {
        if (this.event[cmd]) {
            for (var i = 0; i < this.event[cmd].length; ++i)
                this.event[cmd][i].func.call(this.event[cmd][i].func_obj, data);
        }
    }
}