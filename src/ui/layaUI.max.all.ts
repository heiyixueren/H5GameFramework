
import View=laya.ui.View;
import Dialog=laya.ui.Dialog;
module ui.customView {
    export class marqueeUI extends View {
		public panel_announcement:Laya.Panel;
		public label_announcement:Laya.Label;

        public static  uiView:any ={"type":"View","props":{"width":300,"height":50},"child":[{"type":"Panel","props":{"y":0,"x":0,"width":300,"var":"panel_announcement","height":50},"child":[{"type":"Label","props":{"y":0,"x":0,"var":"label_announcement","valign":"middle","text":"这是公告啦啦啦啦---这是公告啦啦啦啦---这是公告啦啦啦啦---这是公告啦啦啦啦---这是公告啦啦啦啦","pivotY":0,"pivotX":0,"overflow":"visible","height":50,"fontSize":20,"font":"Microsoft YaHei","color":"#dec2af","bottom":0,"align":"left"}}]}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.customView.marqueeUI.uiView);
        }
    }
}

module ui.customView {
    export class toggleButtonUI extends View {
		public view_bg:Laya.Image;
		public view_first:View;
		public img_first:Laya.Image;
		public img_dot:Laya.Image;

        public static  uiView:any ={"type":"View","props":{"width":129,"height":46},"child":[{"type":"Panel","props":{"width":129,"height":46},"child":[{"type":"Image","props":{"y":3,"x":0,"var":"view_bg","skin":"hall/progress_bg.png"}},{"type":"View","props":{"y":0,"x":0,"var":"view_first"},"child":[{"type":"Image","props":{"y":4,"x":1,"width":126,"var":"img_first","skin":"hall/progress_visit.png","sizeGrid":"0,51,0,61","height":38}},{"type":"Image","props":{"y":0,"x":83,"var":"img_dot","skin":"hall/progress_dot.png"}}]}]}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.customView.toggleButtonUI.uiView);
        }
    }
}

module ui.hall {
    export class hallUI extends View {
		public img_background:Laya.Image;
		public view_play_game:View;
		public open_play:Laya.Image;
		public not_open_1:Laya.Image;
		public not_open_2:Laya.Image;
		public not_open_3:Laya.Image;
		public view_other:View;
		public conversion_gold:Laya.Image;
		public luck_corona:Laya.Image;
		public vip_show:Laya.Image;
		public view_setting:View;
		public icon_service:Laya.Image;
		public icon_mail:Laya.Image;
		public icon_setting:Laya.Image;
		public view_me_message:View;
		public avater_bottom:Laya.Image;
		public avater_frame:Laya.Image;
		public avater:Laya.Image;
		public player_name:Laya.Label;
		public gold_count:Laya.Label;
		public view_announcement:View;
		public panel_announcement:Laya.Panel;
		public announcement:Laya.Label;
		public view_right_bottom:View;
		public conversion:Laya.Image;
		public scoreboard:Laya.Image;
		public recharge:Laya.Image;
		public fast_game:Laya.Image;

        public static  uiView:any ={"type":"View","props":{"width":1280,"height":720},"child":[{"type":"Image","props":{"y":0,"x":0,"width":1280,"var":"img_background","skin":"hall/hall_bg.jpg","height":720}},{"type":"View","props":{"y":120,"x":540,"width":489,"var":"view_play_game","height":464},"child":[{"type":"Image","props":{"y":0,"x":0,"var":"open_play","skin":"hall/open.png"}},{"type":"Image","props":{"y":232,"x":0,"var":"not_open_1","skin":"hall/not_open.png"}},{"type":"Image","props":{"y":0,"x":261,"var":"not_open_2","skin":"hall/not_open.png"}},{"type":"Image","props":{"y":232,"x":261,"var":"not_open_3","skin":"hall/not_open.png"}}]},{"type":"View","props":{"y":160,"x":1159,"width":91,"var":"view_other","height":356},"child":[{"type":"Image","props":{"y":0,"x":0,"var":"conversion_gold","skin":"hall/convert_gold.png"}},{"type":"Image","props":{"y":125,"x":0,"var":"luck_corona","skin":"hall/luck_corona.png"}},{"type":"Image","props":{"y":247,"x":0,"var":"vip_show","skin":"hall/vip_show.png"}}]},{"type":"View","props":{"y":0,"x":990,"width":266,"var":"view_setting","height":74},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"hall/cut_off_rule.png"}},{"type":"Image","props":{"y":8,"x":26,"var":"icon_service","skin":"hall/icon_service.png"}},{"type":"Image","props":{"y":0,"x":94,"skin":"hall/cut_off_rule.png"}},{"type":"Image","props":{"y":8,"x":121,"var":"icon_mail","skin":"hall/icon_mail.png"}},{"type":"Image","props":{"y":0,"x":195,"skin":"hall/cut_off_rule.png"}},{"type":"Image","props":{"y":8,"x":222,"var":"icon_setting","skin":"hall/icon_setting.png"}}]},{"type":"View","props":{"y":600,"x":0,"width":407,"var":"view_me_message","height":119},"child":[{"type":"Image","props":{"y":19,"x":0,"var":"avater_bottom","skin":"hall/role_bottom.png"}},{"type":"Image","props":{"y":0,"x":23,"var":"avater_frame","skin":"hall/avater_small.png"}},{"type":"Image","props":{"y":2,"x":25,"width":100,"var":"avater","height":100}},{"type":"Label","props":{"y":28,"x":146,"width":221,"var":"player_name","text":"快递员如东","height":32,"fontSize":24,"font":"Microsoft YaHei","color":"#ffffff"}},{"type":"Image","props":{"y":60,"x":146,"width":195,"skin":"hall/gold_bottom.png","height":46}},{"type":"Image","props":{"y":58,"x":137,"skin":"hall/gold.png"}},{"type":"Label","props":{"y":69,"x":189,"width":131,"var":"gold_count","text":"123666","pivotY":1.3157894736841627,"height":30,"fontSize":20,"color":"#decd95"}}]},{"type":"View","props":{"var":"view_announcement"},"child":[{"type":"Image","props":{"y":0,"x":0,"width":1280,"skin":"hall/info_top.png"}},{"type":"Image","props":{"y":8,"x":300,"width":586,"skin":"hall/announcement_bottom.png","height":46}},{"type":"Image","props":{"y":11,"x":308,"skin":"hall/trumpet.png"}},{"type":"Panel","props":{"y":16,"x":362,"width":518,"var":"panel_announcement","mouseEnabled":false,"height":31,"hScrollBarSkin":"room/transparency.png"},"child":[{"type":"Label","props":{"y":0,"x":18,"width":0,"var":"announcement","text":"公告消息：巴拉巴拉巴拉一大","overflow":"visible","height":0,"fontSize":20,"font":"Microsoft YaHei","color":"#c6c6c6","bold":false}}]}]},{"type":"View","props":{"y":620,"x":873,"width":407,"var":"view_right_bottom","height":100},"child":[{"type":"Image","props":{"y":0,"x":407,"skin":"hall/role_bottom.png","scaleX":-1}},{"type":"Image","props":{"y":3,"x":76,"var":"conversion","skin":"hall/conversion.png"}},{"type":"Image","props":{"y":3,"x":186,"var":"scoreboard","skin":"hall/scoreboard.png"}},{"type":"Image","props":{"y":3,"x":294,"var":"recharge","skin":"hall/recharge.png"}}]},{"type":"Image","props":{"y":609.5,"x":516.5,"var":"fast_game","skin":"hall/fast_game.png"}}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.hall.hallUI.uiView);
        }
    }
}

module ui.hall.setting {
    export class settingUI extends Dialog {

        public static  uiView:any ={"type":"Dialog","props":{"width":1020,"height":586},"child":[{"type":"View","props":{"width":1020,"height":586,"alpha":0.8},"child":[{"type":"Rect","props":{"width":1020,"lineWidth":1,"height":586,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":10,"x":10,"skin":"login/dialog_phone.png","sizeGrid":"100,0,100,0"}},{"type":"View","props":{"y":46,"x":214,"width":603,"height":58},"child":[{"type":"Image","props":{"y":0,"x":441,"skin":"login/dialog_decorate.png"}},{"type":"Image","props":{"y":0,"x":129,"skin":"login/dialog_decorate.png","scaleX":-1}},{"type":"Image","props":{"y":10.5,"x":242,"width":98,"skin":"hall/setting_title.png","height":44}}]},{"type":"Image","props":{"y":42,"x":948,"skin":"login/close.png"}},{"type":"View","props":{"y":153,"x":134,"width":775,"height":140}}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.hall.setting.settingUI.uiView);
        }
    }
}

module ui.hall {
    export class view_announcementUI extends View {
		public img_peri:Laya.Image;
		public announcement_title:Laya.Image;
		public img_bg:Laya.Image;
		public label_announcement:Laya.Label;

        public static  uiView:any ={"type":"View","props":{"width":1280,"height":720,"alpha":1},"child":[{"type":"View","props":{"alpha":0.8},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":1280,"lineWidth":1,"height":720,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":86,"x":11,"width":464,"var":"img_peri","skin":"hall/peri.png","height":634}},{"type":"Image","props":{"y":99,"x":448,"width":745,"var":"announcement_title","skin":"hall/announcement_bg.png","height":517}},{"type":"Image","props":{"y":124,"x":682,"var":"img_bg","skin":"hall/announcement_title.png"}},{"type":"Label","props":{"y":185,"x":525.5,"wordWrap":true,"width":628,"var":"label_announcement","overflow":"scroll","leading":4,"height":394,"fontSize":22,"font":"Microsoft YaHei","color":"#dcd2af"}}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.hall.view_announcementUI.uiView);
        }
    }
}

module ui.login {
    export class loginUI extends View {
		public img_bg:Laya.Image;
		public view_qq_login:View;
		public label_qq_login:Laya.Label;
		public view_we_chat_login:View;
		public label_we_chat_login:Laya.Label;
		public view_phone_login:View;
		public label_phone_login:Laya.Label;
		public login:Laya.Button;
		public register:Laya.Button;
		public password:Laya.TextInput;
		public account:Laya.TextInput;
		public fast_login:Laya.Button;

        public static  uiView:any ={"type":"View","props":{"width":1280,"visible":true,"height":720,"alpha":1},"child":[{"type":"Image","props":{"y":0,"x":0,"width":1280,"var":"img_bg","skin":"login/login_bg.jpg","height":720}},{"type":"View","props":{"y":534,"x":557,"width":199,"var":"view_qq_login","height":74,"alpha":1},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/qq_login_bg.png"}},{"type":"Image","props":{"y":13.5,"x":17,"skin":"login/qq_login.png"}},{"type":"Label","props":{"y":20,"x":81,"width":93,"var":"label_qq_login","text":"QQ登录","stroke":1,"height":34,"fontSize":25,"font":"Microsoft YaHei","color":"#ffffff"}}]},{"type":"View","props":{"y":534,"x":267,"width":199,"var":"view_we_chat_login","height":74},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/we_chat_login_bg.png"}},{"type":"Image","props":{"y":13.5,"x":19,"skin":"login/we_chat_login.png"}},{"type":"Label","props":{"y":19.5,"x":79,"width":106,"var":"label_we_chat_login","text":"微信登陆","stroke":1,"height":35,"fontSize":25,"font":"Microsoft YaHei","color":"#ffffff"}}]},{"type":"View","props":{"y":534,"x":847,"width":199,"var":"view_phone_login","height":74},"child":[{"type":"Image","props":{"skin":"login/phone_login_bg.png"}},{"type":"Image","props":{"y":13.5,"x":15,"skin":"login/phone_login.png"}},{"type":"Label","props":{"y":19.5,"x":74,"width":106,"var":"label_phone_login","text":"手机登陆","stroke":1,"height":35,"fontSize":25,"font":"Microsoft YaHei","color":"#ffffff"}}]},{"type":"View","props":{"y":369,"x":425,"visible":false},"child":[{"type":"Button","props":{"y":94,"x":124,"var":"login","skin":"comp/button.png","label":"登录"}},{"type":"Button","props":{"y":94,"x":240,"var":"register","skin":"comp/button.png","label":"注册"}},{"type":"TextInput","props":{"y":42,"x":126,"var":"password","skin":"comp/textinput.png"}},{"type":"TextInput","props":{"y":0,"x":124,"var":"account","skin":"comp/textinput.png"}},{"type":"Button","props":{"y":90,"x":0,"var":"fast_login","skin":"comp/button.png","label":"游客登录"}},{"type":"Label","props":{"y":4,"x":30,"width":34,"text":"帐号","height":20,"color":"#ffffff"}},{"type":"Label","props":{"y":44,"x":28,"width":34,"text":"密码","height":20,"color":"#ffffff"}}]}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.login.loginUI.uiView);
        }
    }
}

module ui.login.phone_dialog {
    export class PhoneLoginUI extends Dialog {
		public input_phone_number:Laya.TextInput;
		public input_confirm_phone:Laya.TextInput;
		public input_password:Laya.TextInput;
		public img_close:Laya.Image;

        public static  uiView:any ={"type":"Dialog","props":{"y":0,"x":0,"width":1020,"height":586},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/dialog_phone.png"}},{"type":"View","props":{"y":36,"x":204.5,"width":603,"height":58},"child":[{"type":"Image","props":{"y":0,"x":441,"skin":"login/dialog_decorate.png"}},{"type":"Image","props":{"y":0,"x":129,"skin":"login/dialog_decorate.png","scaleX":-1}},{"type":"Image","props":{"y":8.5,"x":197,"width":173,"skin":"login/register_phone.png","height":48}}]},{"type":"Label","props":{"y":133,"x":326,"width":360,"text":"注册不收取任何费用，成功注册额外多送金币","stroke":1,"height":31,"fontSize":18,"font":"Microsoft YaHei","color":"#a59978"}},{"type":"View","props":{"y":210,"x":167},"child":[{"type":"Label","props":{"y":4,"x":100,"width":92,"text":"手机号码：","height":30,"fontSize":20,"font":"Microsoft YaHei","color":"#dcd2af"}},{"type":"TextInput","props":{"y":-4,"x":224,"width":376,"var":"input_phone_number","valign":"middle","skin":"login/input_bg.png","sizeGrid":"5,5,5,5","promptColor":"#a59987","prompt":"在此输入您的手机号码","padding":"0,0,5,10","height":46,"fontSize":20,"font":"Microsoft YaHei"}},{"type":"TextInput","props":{"y":70,"x":224,"width":376,"var":"input_confirm_phone","valign":"middle","skin":"login/input_bg.png","sizeGrid":"5,5,5,5","promptColor":"#fffa7e","prompt":"再次输入手机号码","padding":"0,0,5,10","height":46,"fontSize":20,"font":"Microsoft YaHei"}}]},{"type":"View","props":{"y":351,"x":166},"child":[{"type":"Label","props":{"y":4,"x":100,"width":92,"text":"密       码：","height":30,"fontSize":20,"font":"Microsoft YaHei","color":"#dcd2af"}},{"type":"TextInput","props":{"y":-4,"x":224,"width":376,"var":"input_password","valign":"middle","skin":"login/input_bg.png","sizeGrid":"5,5,5,5","promptColor":"#a59987","prompt":"您的登录密码","padding":"0,0,5,10","height":46,"fontSize":20,"font":"Microsoft YaHei"}}]},{"type":"Image","props":{"y":22,"x":928,"var":"img_close","skin":"login/close.png"}},{"type":"View","props":{"y":429,"x":407},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/phone_login_bg.png"}},{"type":"Label","props":{"y":16,"x":69,"width":61,"text":"登  录","strokeColor":"#c56200","stroke":1,"height":42,"fontSize":26,"font":"Microsoft YaHei","color":"#ffeec1"}}]}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.login.phone_dialog.PhoneLoginUI.uiView);
        }
    }
}

module ui.login.phone_dialog {
    export class PhoneRegisterUI extends Dialog {
		public input_phone_number:Laya.TextInput;
		public input_confirm_phone:Laya.TextInput;
		public input_password:Laya.TextInput;
		public img_close:Laya.Image;
		public view_register:View;

        public static  uiView:any ={"type":"Dialog","props":{"y":0,"x":0,"width":1020,"height":586},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/dialog_phone.png"}},{"type":"View","props":{"y":36,"x":204.5,"width":603,"height":58},"child":[{"type":"Image","props":{"y":0,"x":441,"skin":"login/dialog_decorate.png"}},{"type":"Image","props":{"y":0,"x":129,"skin":"login/dialog_decorate.png","scaleX":-1}},{"type":"Image","props":{"y":8.5,"x":197,"width":173,"skin":"login/register_phone.png","height":48}}]},{"type":"Label","props":{"y":133,"x":326,"width":360,"text":"注册不收取任何费用，成功注册额外多送金币","stroke":1,"height":31,"fontSize":18,"font":"Microsoft YaHei","color":"#a59978"}},{"type":"View","props":{"y":210,"x":167},"child":[{"type":"Label","props":{"y":4,"x":100,"width":92,"text":"手机号码：","height":30,"fontSize":20,"font":"Microsoft YaHei","color":"#dcd2af"}},{"type":"TextInput","props":{"y":-4,"x":224,"width":376,"var":"input_phone_number","valign":"middle","skin":"login/input_bg.png","sizeGrid":"5,5,5,5","promptColor":"#a59987","prompt":"在此输入您的手机号码","padding":"0,0,5,10","height":46,"fontSize":20,"font":"Microsoft YaHei"}},{"type":"TextInput","props":{"y":70,"x":224,"width":376,"var":"input_confirm_phone","valign":"middle","skin":"login/input_bg.png","sizeGrid":"5,5,5,5","promptColor":"#fffa7e","prompt":"再次输入手机号码","padding":"0,0,5,10","height":46,"fontSize":20,"font":"Microsoft YaHei"}}]},{"type":"View","props":{"y":351,"x":166},"child":[{"type":"Label","props":{"y":4,"x":100,"width":92,"text":"密       码：","height":30,"fontSize":20,"font":"Microsoft YaHei","color":"#dcd2af"}},{"type":"TextInput","props":{"y":-4,"x":224,"width":376,"var":"input_password","valign":"middle","skin":"login/input_bg.png","sizeGrid":"5,5,5,5","promptColor":"#a59987","prompt":"您的登录密码","padding":"0,0,5,10","height":46,"fontSize":20,"font":"Microsoft YaHei"}}]},{"type":"Image","props":{"y":22,"x":928,"var":"img_close","skin":"login/close.png"}},{"type":"View","props":{"y":429,"x":407,"var":"view_register"},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/phone_login_bg.png"}},{"type":"Label","props":{"y":16,"x":69,"width":61,"text":"注册","strokeColor":"#c56200","stroke":1,"height":42,"fontSize":26,"font":"Microsoft YaHei","color":"#ffeec1"}}]}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.login.phone_dialog.PhoneRegisterUI.uiView);
        }
    }
}

module ui.login.phone_dialog {
    export class phone_loginUI extends Dialog {
		public img_close:Laya.Image;
		public input_phone_number:Laya.TextInput;
		public input_password:Laya.TextInput;
		public view_forget_password:View;
		public view_register:View;
		public label_qq_login:Laya.Label;
		public view_login:View;

        public static  uiView:any ={"type":"Dialog","props":{"width":1020,"height":586},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/dialog_phone.png","sizeGrid":"100,0,100,0"}},{"type":"View","props":{"y":36,"x":204.5,"width":603,"height":58},"child":[{"type":"Image","props":{"y":0,"x":441,"skin":"login/dialog_decorate.png"}},{"type":"Image","props":{"y":0,"x":129,"skin":"login/dialog_decorate.png","scaleX":-1}},{"type":"Image","props":{"y":8.5,"x":197,"width":173,"skin":"login/phone_login_text.png","height":48}}]},{"type":"Image","props":{"y":32,"x":938,"var":"img_close","skin":"login/close.png"}},{"type":"View","props":{"y":164,"x":156,"width":627,"height":62},"child":[{"type":"Label","props":{"y":11,"x":100,"width":92,"text":"手机号码：","height":30,"fontSize":20,"font":"Microsoft YaHei","color":"#dcd2af"}},{"type":"TextInput","props":{"y":3,"x":224,"width":376,"var":"input_phone_number","valign":"middle","skin":"login/input_bg.png","sizeGrid":"5,5,5,5","promptColor":"#a59987","prompt":"在此输入您的手机号码","padding":"0,0,5,10","height":46,"fontSize":20,"font":"Microsoft YaHei","color":"#ffffff"}}]},{"type":"View","props":{"y":257,"x":156,"width":627,"height":56},"child":[{"type":"Label","props":{"y":11,"x":100,"width":92,"text":"密      码： ","height":30,"fontSize":20,"font":"Microsoft YaHei","color":"#dcd2af"}},{"type":"TextInput","props":{"y":3,"x":224,"width":178,"var":"input_password","valign":"middle","skin":"login/input_bg.png","sizeGrid":"5,5,5,5","promptColor":"#a59987","prompt":"您的登陆密码","padding":"0,0,5,10","height":46,"fontSize":20,"font":"Microsoft YaHei","color":"#ffffff"}}]},{"type":"View","props":{"y":254,"x":582,"width":153,"var":"view_forget_password","height":62,"alpha":1},"child":[{"type":"Image","props":{"y":0,"x":0,"width":150,"skin":"login/qq_login_bg.png","sizeGrid":"0,0,0,0","height":60}},{"type":"Label","props":{"y":13,"x":29,"width":102,"text":"找回密码","strokeColor":"#fc361f","stroke":1,"height":34,"fontSize":22,"font":"Microsoft YaHei","color":"#ffeec1"}}]},{"type":"View","props":{}},{"type":"View","props":{"y":389,"x":270.5,"width":199,"var":"view_register","height":74,"alpha":1},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/qq_login_bg.png"}},{"type":"Label","props":{"y":18.5,"x":70,"width":59,"var":"label_qq_login","text":"注册","strokeColor":"#fc361f","stroke":1,"height":37,"fontSize":26,"font":"Microsoft YaHei","color":"#ffeec1"}}]},{"type":"View","props":{"y":389,"x":544,"width":199,"var":"view_login","height":74,"alpha":1},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/phone_login_bg.png"}},{"type":"Label","props":{"y":18.5,"x":70,"width":59,"text":"登陆","strokeColor":"#c56200","stroke":1,"height":37,"fontSize":26,"font":"Microsoft YaHei","color":"#ffeec1"}}]}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.login.phone_dialog.phone_loginUI.uiView);
        }
    }
}

module ui.room {
    export class HelpDialogUI extends Dialog {
		public img_close:Laya.Image;
		public panel_content:Laya.Panel;
		public html_div:laya.html.dom.HTMLDivElement;

        public static  uiView:any ={"type":"Dialog","props":{"y":0,"x":0,"width":1020,"height":586},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"login/dialog_phone.png"}},{"type":"View","props":{"y":36,"x":204.5,"width":603,"height":58},"child":[{"type":"Image","props":{"y":0,"x":441,"skin":"login/dialog_decorate.png"}},{"type":"Image","props":{"y":0,"x":129,"skin":"login/dialog_decorate.png","scaleX":-1}},{"type":"Image","props":{"y":10,"x":230.5,"width":121,"skin":"room/help_title.png","height":48}}]},{"type":"Image","props":{"y":22,"x":928,"var":"img_close","skin":"login/close.png"}},{"type":"Panel","props":{"y":125,"x":81,"width":850,"var":"panel_content","vScrollBarSkin":"room/transparency.png","height":420},"child":[{"type":"HTMLDivElement","props":{"width":850,"var":"html_div","mouseEnabled":true,"innerHTML":"<div     style=\"color: #dcd2af;font-size: 18px;width: 850px;height: auto;overflow-y: hidden; text-overflow: ellipsis; white-space: normal;overflow-y:auto;line-height:25px;word-wrap:break-word;word-break:break-all;\">     <img src=\"room/adorn.png\"/><h4 style=\"display: inline\"> 扎金花规则:</h4><br/>     1、游戏参与人数和牌数:<br/>游戏参与人数2―5人，使用一副去掉到大小王的扑克牌，共52张牌。<br/>     2、扎金花的玩法流程描述<br/>     投入锅底：发牌之前大家先付出的游戏分。<br/>     发牌：一副牌（52张无大小王），从庄家开始发牌，第一次开局的话，随机选择一个用户为庄家先发牌。每人发三张牌，牌面向下，为暗牌。<br/>     游戏：庄家逆时针的下一家先开始下注，其他玩家依次逆时针操作。轮到玩家操作时，玩家根据条件和判断形势可以进行加、跟、看牌、放弃、比牌等操作。<br/>     <img src=\"room/adorn.png\"/><h4 style=\"display: inline\"> 看牌</h4><br/>     1.没有选择“放弃”的玩家，轮到自己动作时，可以选择看自己的牌，看过的牌为明牌。可以在看牌后再选择是否跟、加，或比牌，这时你的牌就是明牌了，跟、加也是按照明牌的规矩跟、加；当然也可以暗牌操作。<br/>     2.游戏中放弃的玩家，在结束游戏前无权查看自己的牌和其他玩家的牌。<br/>     3.游戏结束后，将公开全部玩家的牌！<br/>     <img src=\"room/adorn.png\"/><h4 style=\"display: inline\"> 比牌</h4><br/>     1.从第二轮开始，玩家在投注前可以选择“比牌”。<br/>     2.比牌双方进行比牌，所有玩家看不到两人的牌面。<br/>     3.牌型牌点相同时被比牌方胜。<br/>     <img src=\"room/adorn.png\"/><h4 style=\"display: inline\"> 跟注、加注：</h4><br/>     1.每次跟注、加注，单注均不可超过个人单局约定的“跟注”“加注”上限--即游戏房间内最大筹码，也就是顶注。<br/>     <img src=\"room/adorn.png\"/><h4 style=\"display: inline\"> 扎金花规则判定胜负:</h4><br/>     1.豹子〉顺金〉金花〉顺子〉对子〉单张<br/>     2.当豹子存在时，“花色不同235”〉“豹子”<br/>     3.AKQ>KQJ>...234>A23。单牌大小：A>K>Q…..>2。<br/>     4.对子的情况，先比对，再比单。<br/>     5.比牌牌型等大，先开为负<br/> </div>","height":700}}]}]};
        constructor(){ super()}
        createChildren():void {
        			View.regComponent("HTMLDivElement",laya.html.dom.HTMLDivElement);

            super.createChildren();
            this.createView(ui.room.HelpDialogUI.uiView);
        }
    }
}
