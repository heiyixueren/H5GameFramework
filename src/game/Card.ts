/**
* 牌，可更改属性及方法，改成麻将牌
*/
module game {
	//扑克牌花色
	export enum CardSuit {
		/**黑桃*/
		black_heart = 1,
		/**红桃*/
		red_heart,
		/**梅花*/
		spade,
		/** 方块 */
		cube
	}
	//扑克牌字型
	export enum CardNo {
		Aice = 1,//"A"
		Two,//2
		Three,//3
		Four,//4
		Five,//5
		Six,//6
		Seven,//7
		Eight,//8
		Nine,//9
		Ten,//10
		Jack,// "J"
		Queen,//"Q"
		King //"K"
	}
	//单张扑克牌
	export class Card {
		/**花色 */
		public suit: CardSuit;
		/** 数字 */
		public cardno: CardNo;

		constructor(suit: CardSuit, cardno: CardNo) {
			this.suit = suit;
			this.cardno = cardno;
		}
	}
}