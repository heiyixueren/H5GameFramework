/**
 * 游戏大厅类
 */

class LobbyUi extends ui.hall.hallUI {
    /**公告条的宽度 */
    announcementWidth: number;
    /** 能滚动的控件宽度 */
    panelWidth: number;

    constructor() {
        super();
        this.initGameList();
        this.AddEvent();
        this.registerEvent();
    }

    timer: Laya.Timer;
    /**加载公告 */
    initAnnouncement() {
        // Laya.Timer.loop(1000, this, this.startAnnouncement, null, true, false);
        if (this.timer == null) {
            this.timer = new Laya.Timer();
        } else {
            //记录label的宽度
            this.timer.clearAll(this);
        }
        this.panelWidth = this.panel_announcement.width;
        /** 记录文字的宽度 */
        this.announcementWidth = this.announcement.width;
        // console.log("panel的宽度：" + this.panelWidth);
        // console.log("文字的宽度：" + this.announcementWidth);

        this.timer.loop(1, this, this.startAnnouncement, null, true, true);
        this.startAnnouncement();
    }


    startAnnouncement() {
        //this.panel_announcement.scrollTo(this.i, 0);
        /** 滚动文字的末尾 */
        var scrollWidth: number = this.announcement.x + this.announcementWidth;
        //console.log("文字末尾到的地方"+scrollWidth+"");
        /** 滚动一周完成 */
        if (scrollWidth <= -1) {
            this.announcement.x = this.panelWidth;
            // this.announcement.x -= 0.02;
        } else {
            this.announcement.x -= 0.02;
        }

    }

    /**注册事件 */
    private registerEvent() {
        Global.opcode.register_event(Opcode.ME_MESSAGE, this, this.meMessage);
        Global.session.Send(Opcode.ME_MESSAGE, null);
        Global.opcode.register_event(Opcode.LOBBY_GAME_LIST, this, this.gameList);
        Global.session.Send(Opcode.LOBBY_GAME_LIST, null);
        Global.opcode.register_event(Opcode.NOTIFY_COINREFRESH, this, this.refreshGold);
        Global.opcode.register_event(Opcode.ENTER_ROOM, this, this.enterRoom);
        Global.opcode.register_event(Opcode.MARQUEE, this, this.marquee);
        Global.session.Send(Opcode.MARQUEE, null);
    }
    marquee(data: any) {
        this.announcement.text = data.notice;
        this.initAnnouncement();
    }

    /** 加载个人信息 */
    meMessage(data: any) {
        // console.log(data);
        var user: User = new User();
        user.avater = data.avater;//头像
        user.id = data.id;
        user.last_game_id = data.last_game_id;
        user.sex = data.sex;
        user.cmd = data.cmd;
        user.name = data.name;
        Global.user = user;
        if (Global.user.avater == "") {
            if (Global.user.sex = 1) {
                //男头像
                //Laya.loader.load("../laya/assets/avater/gentleman",Laya.Handler.create(this,this.setAvater));
                //this.avater.loadImage("http://localhost:8900/bin/asserts/avater/gentleman.png");
                this.avater.skin = "http://localhost:8900/bin/asserts/avater/gentleman.png";
            } else {
                //女头像
                //Laya.loader.load("../laya/assets/avater/lady.png",Laya.Handler.create(this,this.setAvater));
                this.avater.skin = "http://localhost:8900/bin/asserts/avater/lady.png";
            }

        } else {
            this.avater.skin = Global.user.avater;
        }
        this.player_name.text = Global.user.name;
    }

    setAvater() {

    }

    /**刷新金币 */
    refreshGold(data: any) {
        console.log(data);
    }

    private initGameList() {
        for (var index = 0; index < this.view_play_game.numChildren; index++) {
            (<Laya.Image>this.view_play_game.getChildAt(index)).visible = false;
        }
    }

    /** 游戏列表*/
    private gameList(data: any) {
        //console.log(data);
        for (var i: number = 0; i < data.length; ++i) {
            (<Laya.Image>this.view_play_game.getChildAt(i)).visible = true;
            // (<Laya.Image>this.view_play_game.getChildAt(i)).tag = data.game_id;
        }
        switch (data.length) {
            case 1:
                this.not_open_2.visible = true;
                break;
            case 2:
                this.not_open_1.visible = true;
                break;
            case 3:
                this.not_open_3.visible = true;
                break;
            case 4:
                break;
        }
    }

    /** 添加事件 */
    private AddEvent() {
        //console.log(Global.user.cmd);
        //中间四个
        this.open_play.on(Laya.Event.CLICK, this, this.openPlay);
        this.not_open_1.on(Laya.Event.CLICK, this, this.notOpen1);
        this.not_open_2.on(Laya.Event.CLICK, this, this.notOpen2);
        this.not_open_3.on(Laya.Event.CLICK, this, this.notOpen3);
        //上面三个
        this.icon_service.on(Laya.Event.CLICK, this, this.iconService);
        this.icon_mail.on(Laya.Event.CLICK, this, this.iconMail);
        this.icon_setting.on(Laya.Event.CLICK, this, this.iconSetting);
        //右边三个
        this.conversion_gold.on(Laya.Event.CLICK, this, this.conversionGold);
        this.luck_corona.on(Laya.Event.CLICK, this, this.luckCorona);
        this.vip_show.on(Laya.Event.CLICK, this, this.vipShow);
        //右下角三个
        this.conversion.on(Laya.Event.CLICK, this, this.conversionClick);
        this.scoreboard.on(Laya.Event.CLICK, this, this.scoreboardClick);
        this.recharge.on(Laya.Event.CLICK, this, this.rechargeClick);
        //快速游戏
        this.fast_game.on(Laya.Event.CLICK, this, this.fastGame)
    }

    /**快速游戏*/
    fastGame() {
        console.log("快速游戏");
        // this.initAnnouncement();
    }

    /**充值 */
    rechargeClick() {
        let str = "充值";
        console.log('点击了' + str);
    }

    /** 积分榜 */
    scoreboardClick() {
        console.log("积分榜");
    }

    /** 兑换*/
    conversionClick() {
        console.log("兑换");
    }

    /**vip专场 */
    vipShow() {
        console.log("vip专场");
    }

    /** 幸运转盘 */
    luckCorona() {
        console.log("点击了幸运转盘");
    }

    /**兑换金币 */
    conversionGold() {
        console.log("点击兑换金币");
    }

    /**点击设置图标 */
    iconSetting() {
        console.log("点击设置图标");
    }

    /** 点击邮箱图标 */
    iconMail() {
        console.log("点击邮箱图标");
    }

    /**点击客服图标 */
    iconService() {
        console.log("点击客服图标")
    }

    /**点击炸金花 */
    openPlay() {
        // console.log("点击炸金花");
        //this.addChild(new SanZhangRoom());
        // console.log("------------------gameId = "+this.open_play.tag);
        var packed = new Object();
        packed['game_id'] = 1;
        Global.session.Send(Opcode.ENTER_ROOM, packed);
    }
    /**进入房间 */
    enterRoom(data: any) {
        console.log("--------------------" + data.result);
        if (data.result == 0) {
            //进入房间
            this.addChild(new game.RoomBase());
        } else {
            //进入房间失败
        }
    }
    /**点击左下角的未开启 */
    notOpen1() {
        console.log("点击左下角的未开启");
    }
    /** 点击左上角的未开启*/
    notOpen2() {
        console.log("点击右上角的未开启");
    }
    /***点击右下角的未开启 */
    notOpen3() {
        console.log("点击右下角的未开启");
    }



}