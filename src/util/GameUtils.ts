/**游戏工具类 */
class GameUtils { 
    constructor() {
    }

    /**计算自己的位置 */
    public static calculateIndex(index: number):number {
        //得到离自己位置的距离
        var offset:number = 2 - index;
        //真正的位置，不过有可能大于4,或者小于0
        var indeed:number = index + offset;
        var ret:number;
        //如果真正的位置大于4，也就是5，和6就减5
        if (indeed > 4) {
            ret = indeed - 5;
        } else if (indeed < 0) {
            ret = indeed + 5;
        } else {
            ret = indeed;
        }
        return ret;
    }

}