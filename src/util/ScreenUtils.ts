

/**屏幕类 */
class ScreenUtils{
    constructor(){
    }

    public static centerX(parent:View,child:View):number{
        //父容器宽度一半
        var parentWidth = parent.width / 2;
        //子容器宽度一半
        var childWidth = child.width / 2;
        //距离父容器的左边距
        var screenWidth = parentWidth - childWidth;
        //返回x距离
        return screenWidth;
    }

    public static centerY(parent:View,child:View):number{
         //父容器高度一半
        var parentHeight = parent.height / 2;
         //子容器高度一半
        var childHeight = child.height / 2;
        //距离父容器的上边距
        var screenHeight = parentHeight-childHeight;
        //返回y的距离
        return screenHeight;
    }
}
new ScreenUtils;